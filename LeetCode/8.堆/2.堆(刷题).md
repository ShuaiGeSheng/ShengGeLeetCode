## 堆入门练习题
| 题目 | 题解 | 难度 |说明|
| :---| :---: | :---:|:---|
|[506. 相对名次](https://leetcode-cn.com/problems/relative-ranks/)|[题解](https://leetcode-cn.com/problems/relative-ranks/solution/by-goodgoodday-7dvi/)|简单|返回数值得分对应的名次|
|[703. 数据流中的第 K 大元素](https://leetcode-cn.com/problems/kth-largest-element-in-a-stream/)|[题解](https://leetcode-cn.com/problems/kth-largest-element-in-a-stream/solution/by-goodgoodday-cqop/)|简单|返回数据流中第K大的元素|
|[1046. 最后一块石头的重量](https://leetcode-cn.com/problems/last-stone-weight/)|[题解](https://leetcode-cn.com/problems/last-stone-weight/solution/1046-zui-hou-yi-kuai-shi-tou-de-zhong-li-av9y/)|简单|每次选2个石头处理，计算最终剩下的石头的重量|
|[1337. 矩阵中战斗力最弱的 K 行](https://leetcode-cn.com/problems/the-k-weakest-rows-in-a-matrix/)|[题解](https://leetcode-cn.com/problems/the-k-weakest-rows-in-a-matrix/solution/by-goodgoodday-eyif/)|简单|找到最弱的k个兵力下标索引|
|[2099. 找到和最大的长度为 K 的子序列](https://leetcode-cn.com/problems/find-subsequence-of-length-k-with-the-largest-sum/)|[题解](https://leetcode-cn.com/problems/find-subsequence-of-length-k-with-the-largest-sum/solution/by-goodgoodday-5x16/)|简单|找到TopK的最大值|


## 堆基础练习题
| 题目 | 题解 | 难度 |说明|
| :---| :---: | :---:|:---|
|[215. 数组中的第K个最大元素](https://leetcode-cn.com/problems/kth-largest-element-in-an-array/)|[题解](https://leetcode-cn.com/problems/kth-largest-element-in-an-array/solution/by-goodgoodday-fk6j/)|中等|查找数组中第k大的元素|
|[253. 会议室II(会员)](https://leetcode-cn.com/problems/meeting-rooms-ii/)|[题解](https://leetcode-cn.com/problems/meeting-rooms-ii/solution/by-goodgoodday-7jw2/)|中等|给定数组的会议起始时间和结束时间，计算需要多少会议室|
|[264. 丑数 II](https://leetcode-cn.com/problems/ugly-number-ii/)|[题解](https://leetcode-cn.com/problems/ugly-number-ii/solution/by-goodgoodday-q20t/)|中等|找到第n个丑数|
|[378. 有序矩阵中第 K 小的元素](https://leetcode-cn.com/problems/kth-smallest-element-in-a-sorted-matrix/)|[题解](https://leetcode-cn.com/problems/kth-smallest-element-in-a-sorted-matrix/solution/by-goodgoodday-tna9/)|中等|查找到有序矩阵中第k小的元素|
|[658. 找到K个最接近的元素](https://leetcode-cn.com/problems/find-k-closest-elements/)|[题解](https://leetcode-cn.com/problems/find-k-closest-elements/solution/by-goodgoodday-7pwe/)|中等|查找最接近输入参数x的k个元素|
|[659. 分割数组为连续子序列](https://leetcode-cn.com/problems/split-array-into-consecutive-subsequences/)|[题解](https://leetcode-cn.com/problems/split-array-into-consecutive-subsequences/solution/by-goodgoodday-4ifw/)|中等|检查输入的数组`nums`能否切割成至少两个递增的子序列| 
|[692. 前K个高频单词](https://leetcode-cn.com/problems/top-k-frequent-words/)|[题解](https://leetcode-cn.com/problems/top-k-frequent-words/solution/by-goodgoodday-pmpq/)|中等|检查输入的字符串数组中出现频率醉倒的K个字符串| 
|[767. 重构字符串](https://leetcode-cn.com/problems/reorganize-string/)|[题解](https://leetcode-cn.com/problems/reorganize-string/solution/by-goodgoodday-ud3m/)|中等|重构字符串，使输入的字符串中的每个字符两侧都是不相同的字符| 
|[912. 排序数组](https://leetcode-cn.com/problems/sort-an-array/)|[题解](https://leetcode-cn.com/problems/sort-an-array/solution/by-goodgoodday-jetu/)|中等|排序好输入的数组`nums`|
|[973. 最接近原点的K个点](https://leetcode-cn.com/problems/k-closest-points-to-origin/)|[题解](https://leetcode-cn.com/problems/k-closest-points-to-origin/solution/by-goodgoodday-i395/)|中等|求解距离原点距离最小的的k个坐标点|
|[1054. 距离相等的条形码](https://leetcode-cn.com/problems/distant-barcodes/)|[题解](https://leetcode-cn.com/problems/distant-barcodes/solution/by-goodgoodday-ksw8/)|中等|重构数组，使输入的数组中的每个数值两侧都是不相同的数值【767】| 
|[1094. 拼车](https://leetcode-cn.com/problems/car-pooling/)|[题解](https://leetcode-cn.com/problems/car-pooling/solution/by-goodgoodday-8npm/)|中等|输入的参数包括上车的位置，下车位置，人数，载数，检查车能否运送完| 
|[1338. 数组大小减半](https://leetcode-cn.com/problems/reduce-array-size-to-the-half/)|[题解](https://leetcode-cn.com/problems/reduce-array-size-to-the-half/solution/by-goodgoodday-pf6b/)|中等|每次删除数组中相同的数值，最终删除一半元素数组的操作次数|
|[1353. 最多可以参加的会议数目](https://leetcode-cn.com/problems/maximum-number-of-events-that-can-be-attended/)|[题解](https://leetcode-cn.com/problems/maximum-number-of-events-that-can-be-attended/solution/zui-duo-ke-yi-can-jia-de-hui-yi-shu-mu-b-xw5u/)|中等|通过输入的会议的起始结束时间数组，判断能够参加的最多场次|
|[1405. 最长快乐字符串](https://leetcode-cn.com/problems/longest-happy-string/)|[题解](https://leetcode-cn.com/problems/longest-happy-string/solution/by-goodgoodday-loq6/)|中等|通过输入的字符数量构造最大的字符串|
|[1424. 对角线遍历II](https://leetcode-cn.com/problems/diagonal-traverse-ii/)|[题解](https://leetcode-cn.com/problems/diagonal-traverse-ii/solution/by-goodgoodday-1s8g/)|中等|对角线遍历矩阵|
|[1438. 绝对差不超过限制的最长连续子数组](https://leetcode-cn.com/problems/longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit/)|[题解](https://leetcode-cn.com/problems/longest-continuous-subarray-with-absolute-diff-less-than-or-equal-to-limit/solution/by-goodgoodday-pyb3/)|中等|获取数组中不超过限制长度的连续子数组最大的长度|
|[1642. 可以到达的最远建筑](https://leetcode-cn.com/problems/furthest-building-you-can-reach/)|[题解](https://leetcode-cn.com/problems/furthest-building-you-can-reach/solution/chun-cxiao-gen-dui-tan-xin-ke-yi-dao-da-de-zui-yua/)|中等|利用给的梯子数和砖块树计算能够达到的最远位置|
|[1705. 吃苹果的最大数目](https://leetcode-cn.com/problems/maximum-number-of-eaten-apples/)|[题解](https://leetcode-cn.com/problems/maximum-number-of-eaten-apples/solution/chi-ping-guo-zui-da-shu-mu-cyu-yan-xiang-s5yi/)|中等|计算能够吃到的苹果数量|
|[1738. 找出第K大的异或坐标值](https://leetcode-cn.com/problems/find-kth-largest-xor-coordinate-value/)|[题解](https://leetcode-cn.com/problems/find-kth-largest-xor-coordinate-value/solution/zhao-chu-di-k-da-de-yi-huo-zuo-biao-zhi-lqs49/)|中等|找到二维矩阵按规律异或得到第k大值|
|[1753. 移除石子的最大得分](https://leetcode-cn.com/problems/maximum-score-from-removing-stones/)|[题解](https://leetcode-cn.com/problems/maximum-average-pass-ratio/solution/by-goodgoodday-sl2o/)|中等|给班级考试人数，及格人数，再给几个无敌人，加入班级中，计算最大平均值|
|[1792. 最大平均通过率](https://leetcode-cn.com/problems/maximum-average-pass-ratio/)|[题解](https://leetcode-cn.com/problems/maximum-score-from-removing-stones/solution/by-goodgoodday-5xlz/)|中等|给定三堆石头中，每次移除两堆中一个，计算最大得分|
|[1801. 积压订单中的订单总数](https://leetcode-cn.com/problems/number-of-orders-in-the-backlog/)|[题解](https://leetcode-cn.com/problems/number-of-orders-in-the-backlog/solution/gss-by-ac_oier-4pqk/)|中等|给定的卖出订单和买入订单，计算无法完成交易的叮订单数量|
|[1845. 座位预约管理系统](https://leetcode-cn.com/problems/seat-reservation-manager/)|[题解](https://leetcode-cn.com/problems/seat-reservation-manager/solution/by-goodgoodday-rdw5/)|中等|每次根据要求获取最小的座位号|
|[1962. 移除石子使总数最小](https://leetcode-cn.com/problems/seat-reservation-manager/)|[题解](https://leetcode-cn.com/problems/remove-stones-to-minimize-the-total/solution/by-goodgoodday-csz4/)|中等|给定数量堆石子，每次拿走某堆中的一半，求k次后最小的石子数量|
|[1985. 找出数组中的第K大整数](https://leetcode-cn.com/problems/find-the-kth-largest-integer-in-the-array/)|[题解](https://leetcode-cn.com/problems/find-the-kth-largest-integer-in-the-array/solution/by-goodgoodday-wace/)|中等|给的字符串数值，找到其中第k大的字符串数值|
|[2208. 将数组和减半的最少操作次数](https://leetcode-cn.com/problems/minimum-operations-to-halve-array-sum/)|[题解](https://leetcode-cn.com/problems/minimum-operations-to-halve-array-sum/solution/by-goodgoodday-dvb7/)|中等|给定的数组，每次将其中一个元素减半，计算多少次数组和减半|
|[6039.K次增加后的最大乘积](https://leetcode-cn.com/problems/maximum-product-after-k-increments/)|[题解](https://leetcode-cn.com/problems/maximum-product-after-k-increments/solution/by-goodgoodday-5kl4/)|中等|计算输入的数组，自增增k个值后，得到的最大乘积|



## 堆进阶练习题
| 题目 | 题解 | 难度 |说明|
| :---| :---: | :---:|:---|
|[23. 合并K个升序链表](https://leetcode-cn.com/problems/merge-k-sorted-lists/)|[题解](https://leetcode-cn.com/problems/merge-k-sorted-lists/solution/he-bing-kge-sheng-xu-lian-biao-cyu-yan-x-umkb/)|困难|将K个链表表组成1个链表|
|[218. 天际线问题](https://leetcode-cn.com/problems/the-skyline-problem/)|[题解]|困难|给定的城市高度和外观左边图，得出城市的天际线|
|[272. 最接近的二叉搜索树值II(会员)](https://leetcode-cn.com/problems/closest-binary-search-tree-value-ii/)|[题解](https://leetcode-cn.com/problems/closest-binary-search-tree-value-ii/solution/by-goodgoodday-jy30/)|困难|给定的二叉搜索树中，找到最接近target的值最接近的k个值|
|[239. 滑动窗口最大值](https://leetcode-cn.com/problems/sliding-window-maximum/)|[题解](https://leetcode-cn.com/problems/sliding-window-maximum/solution/hua-dong-chuang-kou-zui-da-zhi-by-goodgo-oykb/) |困难|计算大小为`k`的窗口最大值|
|[295. 数据流的中位数](https://leetcode-cn.com/problems/find-median-from-data-stream/)|[题解](https://leetcode-cn.com/problems/find-median-from-data-stream/solution/by-goodgoodday-rcen/)|困难|设计一个数据结构在O(1)的时间复杂度返回数据的中位数|
|[358. K 距离间隔重排字符串](https://leetcode-cn.com/problems/rearrange-string-k-distance-apart/)|[题解](https://leetcode-cn.com/problems/rearrange-string-k-distance-apart/solution/by-goodgoodday-nrfw/)|困难|将字符串重新排列，保证相同的字符之间的间隔不小于`k`|
