## 排序算法
排序算法是将一组数据，按照特定的规则交换位置，是数据具有单调关系(单调递增或单调递减)。

## 排序算法的分类
|排序种类|时间复杂度|最好情况|最坏情况|空间复杂度|排序方式|稳定性|
|:---:|:---:| :---:|:---:|:---:|:---:|:---:|
|冒泡排序|$O(n^2)$|$O(n)$|$O(n)$|$O(1)$|in-place|稳定|
|选择排序|$O(n^2)$|$O(n^2)$|$O(n^2)$|$O(1)$|in-place|不稳定|
|插入排序|$O(n^2)$|$O(n)$|$O(n^2)$|$O(1)$|in-place|稳定|
|希尔排序|$O(nlogn)$|$O(nlog^2n)$|$O(nlog^2n)$|$O(1)$|in-place|不稳定|
|归并排序|$O(nlogn)$|$O(nlogn)$|$O(nlogn)$|$O(n)$|Out-place|稳定|
|快速排序|$O(nlogn)$|$O(nlogn)$|$O(n^2)$|$O(logn)$|in-place|不稳定|
|堆排序|$O(nlogn)$|$O(nlogn)$|$O(nlogn)$|$O(1)$|in-place|不稳定|
|计数排序|$O(n + k)$|$O(n + k)$|$O(n + k)$|$O(k)$|Out-place|不稳定|
|桶排序|$O(n + k)$|$O(n + k)$|$O(n^2)$|$O(n + k)$|Out-place|稳定|
|基数排序|$O(n x k)$|$O(n x k)$|$O(n x k)$|$O(n + k)$|Out-place|稳定|
### 冒泡排序

### 插入排序(Inser Sort)
将数组中元素逐一与已完成排序的元素进行比较。每一步将一个待排序的元素，按大小插入前面已经排序的序列中的适合的位置上，知道全部插入完毕。


使用场合：数据量不大，堆算法稳定性有要求，且局部或整体已经有序的情况。

```c
void InsertSort(int* nums,int numsSize)
{
    int i,j,key;
    for( i =1; i < numsSize; i++)
    {
        //遍历数据
        key = nums[i];   //key用于暂存元素值
        j = i -1;      
        //循环排序，判断条件是元素的下标值大于等于0，
        //并且暂存元素值小于原元素值
        while((j >= 0) && (nums[j] > key))
        {
            //将所有的元素往后移一位
            //下标减1
            nums[j + 1] = nums[j];
            j--;
        }
        nums[j + 1] = key;
        //将值最小元素掺入到最前一个位置
        nums[j +1] = key;
    }
}

案例： [5,2,3,1]
首选开始: 
1:
i = 1; j = 0;  key = 2
start j == 0
{
    i >=0 &&    nums[1] > key
        nums[1]  = nums[0] = 5;
        j--;
}
end : j = -1

nums[j+1]  = key;
[2,5,3,1]

2:
i = 2; j = 1; key = 3
start j == 1
{
    i >=0 &&    nums[1] > key
        nums[2]  = nums[1] = 5;
        j--;
}
end : j = 0
[2,3,5,1]



3:
i = 3; j = 2; key = 1
start j == 1
{
    i >=0 &&    nums[1] > key
        nums[3]  = nums[2] = 5;
        nums[2]  = nums[1] = 3;
        nums[1]  = nums[0] = 2;
        j--;
}
end : j = 0
[1,2,3,5]
```
时间复杂度:$O(n^2)$
空间复杂度:$O(1)$





```c
void my_qsort(int *a,int left,int right)//快速排序函数
{
    /*
    快速排序
    */
    if(left > right)
    {
        return;
    }

    int temp = a[left];
    int i=left;
    int j=right;
    int t;
    while(i!=j)
    {
        while(i<j && a[j]>=temp)
        {
            j--;
        }

        while(i<j&&a[i]<=temp)
        {
            i++;
        }

        if(i<j)
        {
            t=a[i];
            a[i]=a[j];
            a[j]=t;
        }
    }
    a[left]=a[i];
    a[i]=temp;
    my_qsort(a,left, i-1);   //递归
    my_qsort(a,i+1, right);  //递归
}

```