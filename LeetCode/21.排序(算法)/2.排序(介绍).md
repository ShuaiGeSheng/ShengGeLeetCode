
#### 排序入门练习题
| 题目 | 题解 | 难度 |说明|
| :---| :---: | :---:|:---|
|[912. 排序数组](https://leetcode-cn.com/problems/sort-an-array/)|[题解](https://leetcode-cn.com/problems/sort-an-array/solution/by-goodgoodday-jetu/)|中等|排序好输入的数组`nums`|