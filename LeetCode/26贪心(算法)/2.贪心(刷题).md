## 贪心算法入门练习题
| 题目 | 题解 | 难度 |说明|
| :---| :---: | :---:|:---|
|[561. 数组拆分I](https://leetcode-cn.com/problems/array-partition-i/)|[题解](https://leetcode-cn.com/problems/array-partition-i/solution/by-goodgoodday-jeoz/)|简单|将数组来两辆拆分，求数组拆分后符合条件的最大值|
|[860. 柠檬水找零](https://leetcode-cn.com/problems/lemonade-change/)|[题解](https://leetcode-cn.com/problems/lemonade-change/solution/by-goodgoodday-appu/)|简单|检查找零操作是否能完成|
|[2016. 增量元素之间的最大差值](https://leetcode-cn.com/problems/maximum-difference-between-increasing-elements/)|[题解](https://leetcode-cn.com/problems/maximum-difference-between-increasing-elements/solution/zeng-liang-yuan-su-zhi-jian-de-zui-da-ch-brcv/)|简单|找到符合条件的最大差值|


## 贪心算法基础练习题
|[334. 递增的三元子序列](https://leetcode-cn.com/problems/increasing-triplet-subsequence/)|[题解](https://leetcode-cn.com/problems/increasing-triplet-subsequence/solution/di-zeng-de-san-yuan-zi-xu-lie-cyu-yan-xi-qxfs/)|中等|检查数组中是否存在递增的三元数组|
|[2038. 如果相邻两个颜色均相同则删除当前颜色](https://leetcode-cn.com/problems/remove-colored-pieces-if-both-neighbors-are-the-same-color/)|[题解](https://leetcode-cn.com/problems/remove-colored-pieces-if-both-neighbors-are-the-same-color/solution/by-goodgoodday-n4jc/)|中等|轮流删除单独的颜色|
|[2178. 拆分成最多数目的正偶数之和](https://leetcode-cn.com/problems/maximum-split-of-positive-even-integers/)|[题解](https://leetcode-cn.com/problems/maximum-split-of-positive-even-integers/solution/chai-fen-cheng-zui-duo-shu-mu-de-ou-zhen-fgy2/)|中等|将数值才分成尽可能多的偶数之和|
|[6062. 设计一个ATM机器](https://leetcode-cn.com/problems/design-an-atm-machine/)|[题解](https://leetcode-cn.com/problems/design-an-atm-machine/solution/by-goodgoodday-brkh/)|中等|设计款ATM存取按照要求|




## 贪心算法进阶练习题
|[780. 到达终点](https://leetcode-cn.com/problems/reaching-points/)|[题解](https://leetcode-cn.com/problems/reaching-points/solution/dao-da-zhong-dian-cyu-yan-xiang-jie-chao-6j2m/)|困难|检查原始坐标互相加，能否到达`target`坐标|


