## 环形链表入门练习题
| 题目 | 题解 | 难度 |说明|
| :---| :---: | :---:|:---|
|[141. 环形链表](https://leetcode-cn.com/problems/linked-list-cycle/)|[题解](https://leetcode-cn.com/problems/linked-list-cycle/solution/huan-xing-lian-biao-cyu-yan-xiang-jie-3c-urmq/)|简单|给定链表，判断是否为存在环|
|[142. 环形链表 II](https://leetcode-cn.com/problems/linked-list-cycle-ii/)|[题解](https://leetcode-cn.com/problems/linked-list-cycle-ii/solution/huan-xing-lian-biao-ii-cyu-yan-xiang-jie-oufh/)|中等|给定链表，判断是否为存在环，存在则返入环的结点|