# 生哥的刷题日志

#### 介绍
| 介绍 | 说明 |
| :---: | :---:|
| 姓名 | 生哥 | 
| 性别 | 男 |
| 学历| 本科 |
| 大学| 电子科技大学 |
| 职位| 单片机工程师、C语言工程师、嵌入式工程师 |
|C语言刷题QQ群|862326801|


#### 项目概要
该项目主要记录生哥于Leetcode的刷题内容

#### 始末
时间初始：2021年12月22日

#### Leetcode个人主页
![个人主页](%E5%9B%BE%E7%89%87/%E5%9B%BE%E7%89%87.png)
单击[Leetcode个人主页](https://leetcode-cn.com/u/goodgoodday/)进行关注
[500题_2022_04_07](https://leetcode-cn.com/circle/discuss/yhJhhi/)

[400题_2022_03_14](https://leetcode-cn.com/circle/discuss/LRZBWN/)

[300题_2022_02_24](https://leetcode-cn.com/circle/discuss/CedSHD/)

[200题_2022_02_03](https://leetcode-cn.com/circle/discuss/rsJgCP/)

[100题_2022_01_18](https://leetcode-cn.com/circle/discuss/VhA1lM/)

#### 说明
该项目主要用于维护Leetcode的题解
同时也不定期更新一些干货内容，对数据结构和算法进行介绍




#### 电脑端访问

![电脑端访问](https://gitee.com/ShuaiGeSheng/ShengGeLeetCode/raw/e950a0b3ffccdf0c741406f528e32d46b9b1f6d7/%E5%9B%BE%E7%89%87/%E7%94%B5%E8%84%91%E7%AB%AF%E8%AE%BF%E9%97%AE%E5%88%B7%E9%A2%98.gif)

#### 手机访问
![手机端访问](https://gitee.com/ShuaiGeSheng/ShengGeLeetCode/raw/e950a0b3ffccdf0c741406f528e32d46b9b1f6d7/%E5%9B%BE%E7%89%87/%E6%89%8B%E6%9C%BA%E7%AB%AF%E8%AE%BF%E9%97%AE%E5%88%B7%E9%A2%98.gif)

#### 刷题目录
![刷题目录](https://gitee.com/ShuaiGeSheng/ShengGeLeetCode/raw/e950a0b3ffccdf0c741406f528e32d46b9b1f6d7/%E5%9B%BE%E7%89%87/%E5%88%B7%E9%A2%98%E7%9B%AE%E5%BD%95.gif)

#### 版权问题
任何不得对内容进行复制，传播，修改。

#### 感谢
感谢Leetcode让我们相遇。